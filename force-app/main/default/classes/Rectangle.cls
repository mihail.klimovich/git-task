public class Rectangle {
    
    public Double length;
    public Double width;

    public void enterValues(Decimal a, Decimal b){
        length = a;
        width = b;
    }
    public void enterValues(Decimal a){
        System.debug('Enter side rectangle');
    }
    public void enterValues(){
        System.debug('Enter sides rectangle');
    }
    public void display(){
        System.debug('length = '+ length);
        System.debug('width = '+ width);
    }
    public void square(){
        if (length != null && width != null){
            Integer result  = Integer.valueOf(length*width);
            System.debug('square = '+result);
        } else { 
            System.debug('Not square');
        }
        
    }
    public void perimeter(){
        if (length != null && width != null){
            Integer result  = Integer.valueOf(2*length+2*width);
            System.debug('perimeter = '+result);
        } else { 
            System.debug('Not perimeter');
        }
    }
}