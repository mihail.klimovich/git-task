public class MyStringProcessor implements StringProcessor{
    
    public String line;
    
    public MyStringProcessor(String line){
        this.line = line;
    }
    
    public String addPrefix (String str){
        line = str+line;
        return line;
    }
     
    public String addPostfix (String str){
        line = line+str;
        return line;    
    }
    
    public String removeWhitespaces (){
        line = line.replaceAll('\\s+', '');    
        return line;
    }
    
    public void updateList (List<String> firstList, List<String> secondList){
        for (String value: firstList){
            line = value;
            line = addPostfix('able');
            secondList.add(line);
        }
        System.debug(secondList);
    }
}